import pandas as pd




def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df
    
def median_of_title(df, title):
  with_title_predicate = df["Name"].str.contains(title, regex=False)
  ages_with_title = df[with_title_predicate]["Age"]
  missed_count = len(ages_with_title.index) - ages_with_title.count()
  return title, missed_count, ages_with_title.median(skipna=True)


def get_filled():
    df = pd.read_csv("train.csv")
    return [median_of_title(df, title) for title in ["Mr.", "Mrs.", "Miss."]]
